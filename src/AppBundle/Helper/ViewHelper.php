<?php

namespace AppBundle\Helper;

use Symfony\Component\Templating\Helper\Helper;

class ViewHelper extends Helper
{
    public function getName() : string
    {
        return 'viewhelper';
    }

    public function renderContext($currentContext, $targetContext) {
        return $currentContext == $targetContext ? 'class="active"' : '';
    }
}