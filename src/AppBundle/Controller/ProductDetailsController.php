<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ProductDetailsController extends Controller
{
    public function loadAction(Request $request)
    {
        return $this->render('productDetails/productDetails.html.php');
    }
}
