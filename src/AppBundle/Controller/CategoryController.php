<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CategoryController extends Controller
{
    public function getAllAction()
    {
        return $this->render(
            'category/all.html.php',
            ['categories' => $this->get('categoryService')->getAll()]
        );
    }
}