<?php

namespace AppBundle\Service;

use AppBundle\DAO\CategoryDAO;

class CategoryService {
    private $categoryDAO;

    public function __construct(CategoryDAO $categoryDAO)
    {
        $this->categoryDAO = $categoryDAO;
    }

    public function getAll() : ?array
    {
        // Get all top level categories including their sub categories.
        return $this->categoryDAO->getByParent(null);
    }
}