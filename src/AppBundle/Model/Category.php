<?php

namespace AppBundle\Model;

class Category
{
    private $id;
    private $name;
    private $displayName;
    private $parentId;
    private $subCategories;

    public function __construct(int $id, string $name, string $displayName, int $parentId, array $subCategories = [])
    {
        $this->id = $id;
        $this->name = $name;
        $this->displayName = $displayName;
        $this->parentId = $parentId;
        $this->subCategories = $subCategories;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setId(int $id) : void
    {
        $this->id = $id;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function setName(string $name) : void
    {
        $this->name = $name;
    }

    public function getDisplayName() : string
    {
        return $this->displayName;
    }

    public function setDisplayName(string $displayName) : void
    {
        $this->displayName = $displayName;
    }

    public function getParentId() : int
    {
        return $this->parentId;
    }

    public function setParentId(int $parentId) : void
    {
        $this->parentId = $parentId;
    }

    public function getSubCategories() : array
    {
        return $this->subCategories;
    }

    public function setSubCategories(array $subCategories) : void
    {
        $this->subCategories = $subCategories;
    }
}