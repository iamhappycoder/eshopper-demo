<?php

namespace AppBundle\Tests\Repository;

class BaseRepositoryTestCase extends \PHPUnit_Extensions_Database_TestCase
{
    protected static $fixtures;
    protected $doctrine;

    public static function setupBeforeClass()
    {
        parent::setUpBeforeClass();
    }

    public function setUp()
    {
        ($kernel = new \AppKernel('test', true))->boot();

        $this->doctrine = $kernel->getContainer()->get('doctrine');

        // Need an instance of Doctrine before this call.
        parent::setUp();
    }

    final public function getConnection() : \PHPUnit_Extensions_Database_DB_DefaultDatabaseConnection
    {
        static $connection = null;

        if ($connection === null) {
            $connection = $this->createDefaultDBConnection(
                // Use the PDO object that is managed by Doctrine instead of instantiating a new one.
                $this->doctrine->getConnection()->getWrappedConnection()
            );
        }

        return $connection;
    }

    public function getDataSet() : \PHPUnit_Extensions_Database_DataSet_CompositeDataSet
    {
        $compositeDataSet = new \PHPUnit_Extensions_Database_DataSet_CompositeDataSet();

        foreach (self::$fixtures as $fixture) {
            $compositeDataSet->addDataSet(new \PHPUnit_Extensions_Database_DataSet_YamlDataSet(__DIR__ . '/../Fixture/' . $fixture . '.yml'));
        }

        return $compositeDataSet;
    }

    public function getSetUpOperation() : \PHPUnit_Extensions_Database_Operation_Composite
    {
        return new \PHPUnit_Extensions_Database_Operation_Composite(array(
            new class(true) extends \PHPUnit_Extensions_Database_Operation_Truncate
            {
                public function execute(\PHPUnit_Extensions_Database_DB_IDatabaseConnection $connection, \PHPUnit_Extensions_Database_DataSet_IDataSet $dataSet)
                {
                    $connection->getConnection()->query('SET foreign_key_checks = 0');
                    parent::execute($connection, $dataSet);
                    $connection->getConnection()->query('SET foreign_key_checks = 1');
                }
            },
            \PHPUnit_Extensions_Database_Operation_Factory::INSERT()
        ));
    }

    public function getTearDownOperation() : \PHPUnit_Extensions_Database_Operation_IDatabaseOperation
    {
        return \PHPUnit_Extensions_Database_Operation_Factory::TRUNCATE();
    }
}