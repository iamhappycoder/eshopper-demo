<?php

namespace AppBundle\Tests\Repository;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Doctrine\DBAL\Driver\Connection;

class BaseRepositoryKTTestCase extends KernelTestCase
{
    use \PHPUnit_Extensions_Database_TestCase_Trait;

    protected $doctrine;
    private $connection;
    private $fixtures;

    public function __construct(array $fixtures, $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->fixtures = $fixtures;
    }

    public function setUp()
    {
        parent::setUp();

        self::bootKernel();

        $this->doctrine = static::$kernel->getContainer()
                                         ->get('doctrine');

        $this->connection = $this->doctrine
                                 ->getConnection()
                                 ->getWrappedConnection();
    }

    public function getDataSet() : \PHPUnit_Extensions_Database_DataSet_CompositeDataSet
    {
        $compositeDataSet = new \PHPUnit_Extensions_Database_DataSet_CompositeDataSet();

        foreach ($this->fixtures as $fixture) {
            $compositeDataSet->addDataSet(
                new \PHPUnit_Extensions_Database_DataSet_YamlDataSet(__DIR__ . '/../Fixture/' . $fixture . '.yml')
            );
        }

        return $compositeDataSet;
    }

    public function getConnection() : Connection
    {
        return $this->connection;
    }

    public function getSetUpOperation() {
        return new \PHPUnit_Extensions_Database_Operation_Composite(array(
            new class(true) extends \PHPUnit_Extensions_Database_Operation_Truncate{
                public function execute(\PHPUnit_Extensions_Database_DB_IDatabaseConnection $connection, \PHPUnit_Extensions_Database_DataSet_IDataSet $dataSet) {
                    $connection->getConnection()->query('SET foreign_key_checks = 0');
                    parent::execute($connection, $dataSet);
                    $connection->getConnection()->query('SET foreign_key_checks = 1');
                }
            },
            \PHPUnit_Extensions_Database_Operation_Factory::INSERT()
        ));
    }
}