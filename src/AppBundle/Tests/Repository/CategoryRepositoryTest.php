<?php

namespace AppBundle\Tests\Repository;

use AppBundle\Entity\Category;
use AppBundle\Tests\Mother\CategoryMother;

class CategoryRepositoryTest extends BaseRepositoryTestCase
{
    private $categoryRepository = null;

    public static function setupBeforeClass()
    {
        parent::setupBeforeClass();

        self::$fixtures = ['Category'];
    }

    public function setUp()
    {
        parent::setUp();

        $this->categoryRepository = $this->doctrine->getRepository('AppBundle:Category');
    }

    public function testGetTopLevelCategories()
    {
        $categories = $this->categoryRepository->findBy(['parent' => null]);

        $actual = $this->convertCategoriesToArray($categories);
        $expected = CategoryMother::getTopLevelCategories();

        $this->assertEquals($expected, $actual);
    }

    public function testGetSubCategory()
    {
        $categories = $this->categoryRepository->findBy(['parent' => 1]);

        $actual = $this->convertCategoriesToArray($categories);
        $expected = CategoryMother::getSubCategory();

        $this->assertEquals($expected, $actual);
    }

    public function testCreateTopLevelCategory()
    {
        $criteria = ['parent' => null, 'name' => 'new-category', 'displayName' => 'New Category'];

        // Pre condition
        $category = $this->categoryRepository->findOneBy($criteria);
        $this->assertNull($category);

        $newCategory = new Category();
        $newCategory->setName('new-category');
        $newCategory->setDisplayName('New Category');
        $newCategory->setParent(null);

        $em = $this->doctrine->getManager();
        $em->persist($newCategory);
        $em->flush();

        // Post condition
        $category = $this->categoryRepository->findOneBy($criteria);
        $this->assertNotNull($category);
    }

    public function testCreateSubCategory()
    {
        // Pre condition
        $category = $this->categoryRepository->findOneBy(['parent' => 1, 'name' => 'new-sub-category', 'displayName' => 'New Sub Category']);
        $this->assertNull($category);

        $newCategory = new Category();
        $newCategory->setName('new-sub-category');
        $newCategory->setDisplayName('New Sub Category');
        $newCategory->setParent($this->categoryRepository->find(1));

        $em = $this->doctrine->getManager();
        $em->persist($newCategory);
        $em->flush();

        // Post condition
        $category = $this->categoryRepository->findOneBy(['parent' => 1, 'name' => 'new-sub-category', 'displayName' => 'New Sub Category']);
        $this->assertNotNull($category);

        $expectedParentCategory = new Category();
        $expectedParentCategory->setName('mens');
        $expectedParentCategory->setDisplayName('Mens');
        $expectedParentCategory->setParent(null);
        $expectedParentCategory->setId(1);

        $expectedCategory = new Category();
        $expectedCategory->setName('new-sub-category');
        $expectedCategory->setDisplayName('New Sub Category');
        $expectedCategory->setParent($expectedParentCategory);
        $expectedCategory->setId(9);
        $this->assertEquals($expectedCategory, $category);
    }

    private function convertCategoriesToArray(array $categories) : array
    {
        $arr = [];

        foreach ($categories as $category) {
            $tmp = [
                'id' => $category->getId(),
                'name' => $category->getName(),
                'displayName' => $category->getDisplayName(),
                'parent' => $category->getParent() !== null ? $category->getParent()->getId() : null,
                'children' => []
            ];

            if ($category->getChildren()->count()) {
                $tmp['children'] = $this->convertCategoriesToArray($category->getChildren()->toArray());
            }

            $arr[] = $tmp;
        }

        return $arr;
    }
}