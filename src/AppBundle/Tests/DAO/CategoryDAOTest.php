<?php

namespace AppBundle\Tests\DAO;

use AppBundle\DAO\CategoryDAO;
use AppBundle\Repository\CategoryRepository;
use Doctrine\Bundle\DoctrineBundle\Registry;
use PHPUnit\Framework\TestCase;

class CategoryDAOTest extends TestCase
{
    public function testGetByParent()
    {
        list ($doctrineMock, $entityRepository) = $this->getMockObjects();

        $entityRepository->expects($this->once())->method('findBy')->with(['parent' => null])->willReturn(['categories']);
        $doctrineMock->expects($this->once())->method('getRepository')->with('AppBundle:Category')->willReturn($entityRepository);

        $categoryDAO = new CategoryDAO($doctrineMock);
        $actual = $categoryDAO->getByParent(null);
        $expected = ['categories'];

        $this->assertEquals($expected, $actual);
    }

    private function getMockObjects() : array
    {
        return [
            $this->createMock(Registry::class),
            $this->createMock(CategoryRepository::class )
        ];
    }
}