<?php

namespace AppBundle\Tests\Mother;

class CategoryMother
{
    public static function getTopLevelCategories() : array
    {
        return [
            ['id' => 1, 'name' => 'mens', 'displayName' => 'Mens', 'parent' => null, 'children' =>
                [
                    ['id' => 2, 'name' => 'mens-shirts', 'displayName' => 'Shirts', 'parent' => 1, 'children' =>
                        [
                            ['id' => 7, 'name' => 'vneck', 'displayName' => 'V Neck', 'parent' => 2, 'children' => []],
                            ['id' => 8, 'name' => 'roundneck', 'displayName' => 'Round Neck', 'parent' => 2, 'children' => []]
                        ]
                    ],
                    ['id' => 3, 'name' => 'mens-pants', 'displayName' => 'Pants', 'parent' => 1, 'children' => []]
                ]
            ],
            ['id' => 4, 'name' => 'womens', 'displayName' => 'Womens', 'parent' => null, 'children' =>
                [
                    ['id' => 5, 'name' => 'womens-shirts', 'displayName' => 'Shirts', 'parent' => 4, 'children' => []],
                    ['id' => 6, 'name' => 'womens-pants', 'displayName' => 'Pants', 'parent' => 4, 'children' => []]
                ]
            ]
        ];
    }

    public static function getSubCategory() : array
    {
        return [
            ['id' => 2, 'name' => 'mens-shirts', 'displayName' => 'Shirts', 'parent' => 1, 'children' =>
                [
                    ['id' => 7, 'name' => 'vneck', 'displayName' => 'V Neck', 'parent' => 2, 'children' => []],
                    ['id' => 8, 'name' => 'roundneck', 'displayName' => 'Round Neck', 'parent' => 2, 'children' => []]
                ]
            ],
            ['id' => 3, 'name' => 'mens-pants', 'displayName' => 'Pants', 'parent' => 1, 'children' => []]
        ];
    }
}