<?php

namespace AppBundle\DAO;

class CategoryDAO extends BaseDAO
{
    private const OBJECT_NAME = 'AppBundle:Category';

    public function getByParent(?int $id) : ?array
    {
        return $this->doctrine->getRepository(self::OBJECT_NAME)->findBy(['parent' => $id]);
    }
}