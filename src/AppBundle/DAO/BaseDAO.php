<?php

namespace AppBundle\DAO;

use Doctrine\Bundle\DoctrineBundle\Registry;

class BaseDAO
{
    protected $doctrine;

    public function __construct(Registry $doctrine)
    {
        $this->doctrine = $doctrine;
    }
}