<div class="panel-group category-products" id="accordian"><!--category-productsr-->
    <?php foreach ($categories as $category): ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordian" href="#<?php echo $category->getName() ?>">
                        <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                        <?php echo $category->getDisplayName() ?>
                    </a>
                </h4>
            </div>
            <div id="<?php echo $category->getName() ?>" class="panel-collapse collapse">
                <div class="panel-body">
                    <ul>
                        <?php foreach ($category->getChildren() as $subCategory): ?>
                            <?php if ($subCategory->getChildren()): ?>
                                <li><a href="#"><?php echo $subCategory->getDisplayName() ?></a>
                                    <ul>
                                        <?php foreach ($subCategory->getChildren() as $child): ?>
                                            <li><a href="#"><?php echo $child->getDisplayName() ?></a></li>
                                        <?php endforeach ?>
                                    </ul>
                                </li>
                            <?php else: ?>
                                <li><a href="#"><?php echo $subCategory->getDisplayName() ?></a></li>
                            <?php endif ?>
                        <?php endforeach ?>
                    </ul>
                </div>
            </div>
        </div>
    <?php endforeach ?>
</div><!--/category-products-->